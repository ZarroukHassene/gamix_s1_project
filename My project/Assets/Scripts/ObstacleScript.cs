using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ObstacleScript : MonoBehaviour
{
    void OnTriggerEnter(Collider other)
    {   
        //Check ParentObjectCollider collision
        if (other.CompareTag("Player") )
        {
            PlayerManager.Instance.InstaDeath();
            GetComponent<AudioSource>().Play();
            CameraShaker.Invoke();
        }
    }
}
