using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class SoundManager : StaticInstance<SoundManager>
{
    public SoundClip[] musicClips, sfxClips;
    public AudioSource musicSource, sfxSource;

    [SerializeField] GameObject playerObject;   


    private void Start()
    {
        //PlayMusic("Track1");
    }

    public void PlayMusicOneShot(string name)
    {
        SoundClip s = Array.Find(musicClips, sound => sound.name == name);

        if (s == null)
        {
            Debug.LogWarning("Sound: " + name + " not found!");
            return;
        }
        else
        {
            musicSource.clip = s.clip;
            musicSource.Play();
            musicSource.loop = false;
        }


    }

    public void PlayMusicLoop(string name)
    {
        SoundClip s = Array.Find(musicClips, sound => sound.name == name);

        if (s == null)
        {
            Debug.LogWarning("Sound: " + name + " not found!");
            return;
        }
        else
        {
            musicSource.clip = s.clip;
            musicSource.Play();
            musicSource.loop = true;
        }
    }

    public void PlaySFXOneShot(string name)
    {
        SoundClip s = Array.Find(sfxClips, sound => sound.name == name);
        if (s == null)
        {
            Debug.LogWarning("Sound: " + name + " not found!");
            return;
        }
        else
        {
            sfxSource.clip = s.clip;
            sfxSource.Play();
            sfxSource.loop = false;
        }
    }

    public void PlaySFXLoop(string name)
    {
        SoundClip s = Array.Find(sfxClips, sound => sound.name == name);
        if (s == null)
        {
            Debug.LogWarning("Sound: " + name + " not found!");
            return;
        }
        else
        {
            sfxSource.clip = s.clip;
            sfxSource.Play();
            sfxSource.loop = true;
        }
    }

    public void StopMusic(string name)
    {
        SoundClip s = Array.Find(musicClips, sound => sound.name == name);
        if (s == null)
        {
            Debug.LogWarning("Sound: " + name + " not found!");
            return;
        }
        else
        {
            musicSource.Stop();
        }

    }

    public void StopSFX(string name)
    {
        SoundClip s = Array.Find(sfxClips, sound => sound.name == name);
        if (s == null)
        {
            Debug.LogWarning("Sound: " + name + " not found!");
            return;
        }
        else
        {
            sfxSource.Stop();
        }
    }

    public void StopAllMusic()
    {
        musicSource.Stop();
    }

    public void StopAllSFX()
    {
        sfxSource.Stop();
    }   

    public void PauseMusic()
    {
        musicSource.Pause();
    }

    public void PauseSFX()
    {
        sfxSource.Pause();
    }

    public void ResumeMusic()
    {
        musicSource.UnPause();
    }

    public void ResumeSFX()
    {
        sfxSource.UnPause();
    }




    public void handleGameOverSounds()
    {
        StopAllMusic();
        PlaySFXOneShot("BassDrop");
    }

    public void handleWinSounds()
    {
        StopAllMusic();
        //PlaySFX("Win");
    }

    public void handlePauseSounds()
    {
        PauseMusic();
        PauseSFX();
    }

    public void handleInLevelSounds()
    {
        PlayMusicOneShot("Track2");
       // PlaySFXLoop("RunningLoop");
    }

    public void handlePauseAllSounds()
    {
    StopAllMusic();
        StopAllSFX();
    }

}
