using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;
using UnityEngine.Events;
using UnityEngine.UI;

public class UIManager : StaticInstance<UIManager>
{
    TextMeshProUGUI ScoreNumber;
    TextMeshProUGUI HPNumber;
    Canvas gameOverScreen;
    Canvas HUD;
    Canvas pauseScreen;
    Canvas winScreen;

    private void Start()
    {  
        Debug.Log("Entering UIManager Start()"); 

            gameOverScreen = GameObject.Find("GameOverScreen").GetComponent<Canvas>();
            HUD = GameObject.Find("HUD").GetComponent<Canvas>();
            pauseScreen = GameObject.Find("PauseScreen").GetComponent<Canvas>();
            winScreen = GameObject.Find("WinScreen").GetComponent<Canvas>();
            
            ScoreNumber = GameObject.Find("ScoreNumber").GetComponent<TextMeshProUGUI>();
            HPNumber = GameObject.Find("HPNumber").GetComponent<TextMeshProUGUI>();

       
        Debug.Log("Exiting UIManager Start()");
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Pause();
        }
    }

    public void ShowWinScreen()
    {
        HUD.GetComponent<Canvas>().enabled = false;
        winScreen.GetComponent<Canvas>().enabled = true;
    }

    public void ShowHUD()
    {   
        HUD.GetComponent<Canvas>().enabled = true;
    }

    public void ButtonClicked_Resume()
    {
        GameManager.Instance.ChangeState(GameState.InGame);
        HUD.GetComponent<Canvas>().enabled = true;
        pauseScreen.GetComponent<Canvas>().enabled = false;
    }

    public void ButtonClicked_Exit()
    {
        gameOverScreen.GetComponent<Animator>().SetTrigger("Disappear");
        GameManager.Instance.ChangeState(GameState.InMainMenu);
        
    }

    public void ButtonClicked_Retry()
    {
        gameOverScreen.GetComponent<Animator>().SetTrigger("Disappear");
        GameManager.Instance.ChangeState(GameState.StartLevel1);
        //PlayerManager.Instance.ChangeState(PlayerState.Idle);
        //PlayerManager.Instance.anim.SetBool("IsIdle", true);
    }

    public void Pause()
    {
        GameManager.Instance.ChangeState(GameState.Pause);
        ShowPauseMenu();
    }

    public void ShowPauseMenu()
    {
        HUD.GetComponent<Canvas>().enabled = false;
        pauseScreen.GetComponent<Canvas>().enabled = true;
    }

    public void ShowGameOverScreen()
    { 
        gameOverScreen.GetComponent<Canvas>().enabled = true;
        gameOverScreen.GetComponent<Animator>().SetTrigger("Appear");
    }

    public void UpdateScore()
    {
        ScoreNumber.text = GameManager.Instance.score.ToString();
    }

    public void UpdateHP()
    {
        HPNumber.text = PlayerManager.Instance.HP.ToString();
    }
}
