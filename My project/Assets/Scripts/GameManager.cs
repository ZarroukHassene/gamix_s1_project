#pragma warning disable 618
using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

//The player doesn't actually move forward on the Z axis, but the world moves backwards on the Z axis, by design choice
public class GameManager : PersistentSingleton<GameManager>
{
    [SerializeField] public int score = 0;

    public static event Action<GameState> OnBeforeStateChanged;
    public static event Action<GameState> OnAfterStateChanged;

    public GameState State;
    [SerializeField] float countdownDuration = 6.6f;

    //MainCamera
    Camera mainCamera;

    GameObject speedLines, player;

   

    [SerializeField] Vector3 playerStartingPosition= new Vector3(-1.67f, 3.88f, -89.782f);


    void Start()
    {
        Debug.Log("Entering GameManager Start()");


        //Check if active scene is scene named "Level1", if yes, change state to InGame
        if (SceneManager.GetActiveScene().name == "Level1")
        {
            ChangeState(GameState.InCountDown);
        }
        else if (SceneManager.GetActiveScene().name == "MainMenu")
        {
            ChangeState(GameState.InMainMenu);
        }

        // ChangeState(GameState.InGame);
        Debug.Log("Exiting GameManager Start()");
    }

    public void ChangeState(GameState newState)
    {
        if (State != newState)
        {
            OnBeforeStateChanged?.Invoke(newState);

            State = newState;
            Debug.Log("<color=yellow>GAME STATE CHANGED TO [" + newState + "]</color>");
            switch (newState)
            {
                case GameState.Starting:
                    HandleStarting();

                    break;
                case GameState.InMainMenu:
                    HandleInMainMenu();
                    break;
                case GameState.InGame:

                    HandleInGame();
                    break;
                case GameState.Pause:
                    HandlePause();
                    break;
                case GameState.GameOver:
                    HandleGameOver();
                    break;
                case GameState.Win:
                    HandleWin();
                    break;
                case GameState.StartLevel1:
                    if (SceneManager.GetActiveScene().name != "Level1")
                    { HandleStartLevel1(); }
                    else
                    { handleRestartLevel1(); }
                    break;
                case GameState.InCountDown:
                    HandleInCountDown();
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(newState), newState, null);
            }

            OnAfterStateChanged?.Invoke(newState);
            
        }
        else
        {
            Debug.Log("<color=red>Blocked GAMESTATE to change from [" + State + "] to [" + newState + "]</color>");

        }

    }

    public void GetReferences()
    {   //white debug log 
        //Debug.Log("<color=white>Getting references...</color>");
        //Get the main camera
        if (mainCamera == null)
        {
            //Debug.Log("<color=red>mainCamera not found! ...Getting reference...</color>");
            mainCamera = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>();
        }
        else
        {
            //Debug.Log("<color=white>mainCamera found!</color>");
        }
        //Get speedlines VFX
        if (speedLines == null)
        {   
            //Debug.Log("<color=red>speedLines not found! ...Getting reference...</color>");
            speedLines = GameObject.Find("SpeedLines");
        }
        else
        {
            // Debug.Log("<color=white>speedLines found!</color>");
        }
        //Get player
        if (player == null)
        { //Debug.Log("<color=red>player not found! ...Getting reference...</color>");
            player = GameObject.FindGameObjectWithTag("Player");
        }
        else
        {
            //Debug.Log("<color=white>player found!</color>");
        }

    }



    private void HandleStarting()
    {
        Debug.Log("Entering Starting");

        mainCamera.GetComponent<Animator>().applyRootMotion = false;
        ChangeState(GameState.InMainMenu);

        Debug.Log("Exiting Starting");

    }

    private void HandleInMainMenu()
    {
        Debug.Log("Entering InMainMenu");

        if (SceneManager.GetActiveScene().name != "MainMenu")
        {
            SceneManager.LoadScene("MainMenu");
            //BLUE debug log
            Debug.Log("<color=blue>Scene changed to MainMenu</color>");
        }

        Debug.Log("Exiting InMainMenu");
    }


    private void HandleStartLevel1()
    {
        Debug.Log("Entering StartLevel1");

        SceneManager.LoadScene("Level1");
        Debug.Log("<color=blue>Scene changed to Level1</color>");
        ChangeState(GameState.InCountDown);

        Debug.Log("Exiting StartLevel1");
    }

    private void handleRestartLevel1()
    {
        Debug.Log("Entering RESTART StartLevel1");
        GameObject Track = GameObject.Find("Track");
        Track.transform.position = new Vector3(-1.721596f, 2.78464f, 10.81791f);
        PlayerManager.Instance.HP = 100;
        player.transform.position = playerStartingPosition;



        ChangeState(GameState.InCountDown);

        Debug.Log("Exiting RESTART StartLevel1");
    }


    private void HandleInGame()
    {
        Debug.Log("Entering InGame");

        mainCamera.GetComponent<Animator>().applyRootMotion = true;
        mainCamera.GetComponent<Animator>().SetTrigger("Normal");

        UIManager.Instance.ShowHUD();

        speedLines.SetActive(true);
        speedLines.GetComponent<ParticleSystem>().Play();

        PlayerManager.Instance.ChangeState(PlayerState.Running);

        Time.timeScale = 1;
        Debug.Log("Exiting InGame");
    }

    private void HandlePause()
    {
        Debug.Log("Entering Pause");

        speedLines.GetComponent<ParticleSystem>().Stop();

        Time.timeScale = 0;
        UIManager.Instance.ShowPauseMenu();


        Debug.Log("Exiting Pause");
    }

    private void HandleGameOver()
    {
        Debug.Log("Entering Game Over");


        UIManager.Instance.ShowGameOverScreen();

        speedLines.GetComponent<ParticleSystem>().Stop();

        speedLines.SetActive(false);

        Debug.Log("Exiting Game Over");

    }

    private void HandleWin()
    {
        Debug.Log("Entering Win");
        PlayerManager.Instance.ChangeState(PlayerState.Idle);
        UIManager.Instance.ShowWinScreen();
        StartCoroutine(Win());
        speedLines.GetComponent<ParticleSystem>().Stop();
        speedLines.SetActive(false);
        

        Debug.Log("Exiting Win");
    }


    //Event subscriber
    public void IncrementScore()
    {
        score++;
    }


    public void QuitGame()
    {
        Application.Quit();
        Debug.Log("Game Quit");
    }

    //Win coroutine: set animator trigger "Win" then wait 2 seconds, then set animator trigger "Celebrate"
    public IEnumerator Win()
    {
        player.GetComponent<Animator>().SetTrigger("Win");
        mainCamera.GetComponent<Animator>().SetTrigger("Win");

        yield return new WaitForSeconds(2);

        PlayerManager.Instance.ChangeState(PlayerState.Dancing);
    }

    public void HandleInCountDown()
    {
        Debug.Log("Entering InCountDown");
        PlayerManager.Instance.playerState = PlayerState.Idle;
        PlayerManager.Instance.anim.SetBool("IsIdle", true);
        StartCoroutine(CountDown());

        Debug.Log("Exiting InCountDown");
    }

    public IEnumerator CountDown()
    {   Debug.Log("Starting CountDown coroutine");
        //yield return new WaitForSeconds(2);
        GetReferences();

        speedLines.GetComponent<ParticleSystem>().Stop();

        speedLines.SetActive(false);
        yield return new WaitForSeconds(countdownDuration);



        
        PlayerManager.Instance.anim.SetBool("IsIdle", false);
        ChangeState(GameState.InGame);
        Debug.Log("Finished CountDown coroutine");
    }


}


[Serializable]
public enum GameState
{
    Starting = 0,
    InMainMenu = 1,
    InGame = 2,
    Pause = 3,
    GameOver = 4,
    Win = 5,
    StartLevel1 = 6,
    InCountDown = 7,

}
