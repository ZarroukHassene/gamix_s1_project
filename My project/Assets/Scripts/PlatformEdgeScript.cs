using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class PlatformEdgeScript : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Player"))
        {
            PlayerManager.Instance.InstaDeath();
            GetComponent<AudioSource>().Play();
            CameraShaker.Invoke();
        }
    }
}
