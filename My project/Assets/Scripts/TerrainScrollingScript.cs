using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TerrainScrollingScript : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField] public float ScrollingSpeed = 0.04f;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
        
        
        if(GameObject.Find("GameManager").GetComponent<GameManager>().State== GameState.InGame)
        {
          //Translate object on Z axis backwards, depending on the speed and the time between frames
          transform.Translate(0, 0, -ScrollingSpeed * Time.deltaTime);

          
        }

    }

}
