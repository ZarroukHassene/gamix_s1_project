using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class PlayerManager : StaticInstance<PlayerManager>
{
    public static event Action<PlayerState> OnBeforeStateChanged;
    public static event Action<PlayerState> OnAfterStateChanged;
    public PlayerState playerState;

    [SerializeField] public int HP = 100;

    public bool isGrounded;

    Vector3 originalColliderSize;
    Vector3 originalColliderCenter;
    private BoxCollider boxCollider;

    [SerializeField] Vector3 StrafeForce = new Vector3(0.5f, 0, 0);
    [SerializeField] Vector3 AirStrafeForce = new Vector3(0.1f, 0, 0);
    [SerializeField] float JumpForce = 220;


    AudioSource footstepsAudioSource;
    AudioSource slidingAudioSource;
    AudioSource jumpingAudioSource;

    public Animator anim; // Animator of the player
    Rigidbody rb; // Rigidbody of the player

    GameObject player;

    private float horizontalInput;

    PlayerMovementScript playerMovementScript;
    void Start()
    {   //Debug
        Debug.Log("Entering PlayerManager Start()");

        //Get Player
        player = GameObject.FindGameObjectWithTag("Player");
        rb = player.GetComponent<Rigidbody>();
        anim = player.GetComponent<Animator>();

        boxCollider = player.GetComponent<BoxCollider>();

        originalColliderSize = boxCollider.size;

        originalColliderCenter = boxCollider.center;

        footstepsAudioSource = GameObject.Find("FootstepsSource").GetComponent<AudioSource>();
        slidingAudioSource = GameObject.Find("SlidingSource").GetComponent<AudioSource>();
        jumpingAudioSource = GameObject.Find("JumpingSource").GetComponent<AudioSource>();

        playerMovementScript = player.GetComponent<PlayerMovementScript>();

        
        playerState = PlayerState.Idle;
        
        //Debug
        Debug.Log("Exiting PlayerManager Start()");
    }

    void Update()
    {
        if (GameManager.Instance.State == GameState.InGame)
        {
            isGrounded = playerMovementScript.CheckGrounded();


            playerMovementScript.handleTurning();
            //DEBUG LOg in red
            //Debug.Log("<color=red>PlayerManager Update()</color>");



            //JUMPING
            if (Input.GetKeyDown(KeyCode.Space) && isGrounded)
            {   
                ChangeState(PlayerState.Jumping);
            }



            //SLIDING
            if (Input.GetKeyDown(KeyCode.LeftShift) && isGrounded)
            {
                
                ChangeState(PlayerState.Sliding);

            }

            if (GameManager.Instance.State == GameState.GameOver)
            {
                ChangeState(PlayerState.Dead);
            }
            if (GameManager.Instance.State == GameState.Win)
            {
                anim.SetTrigger("Win");
            }




        }

        //Health
        if (HP <= 0)
        {
            Debug.Log("Health reached zero.");
            anim.SetBool("IsDead", true);
            anim.SetBool("IsRunning", false);
            HP++;
            ChangeState(PlayerState.Dead);
            GameManager.Instance.ChangeState(GameState.GameOver);

        }

        //Check if grounded
        isGrounded = playerMovementScript.CheckGrounded();

    }

    public void ChangeState(PlayerState newState)
    {
        if (playerState != newState)
        {
           

            OnBeforeStateChanged?.Invoke(newState);

            playerState = newState;
            Debug.Log("<color=green>PLAYER STATE CHANGED TO [" + newState + "]</color>");
            switch (newState)
            {
                case PlayerState.Idle:
                    playerMovementScript.handleIdle();
                    footstepsAudioSource.Stop();
                    break;
                case PlayerState.Running:
                    playerMovementScript.handleRunning();
                    footstepsAudioSource.enabled = true;
                    footstepsAudioSource.Play();
                    break;
                case PlayerState.Jumping:
                footstepsAudioSource.Stop();
                    jumpingAudioSource.Play();
                    StartCoroutine(playerMovementScript.Jumping());
                    
                    break;
                case PlayerState.Sliding:
                    slidingAudioSource.Play();
                    footstepsAudioSource.Stop();
                    StartCoroutine(playerMovementScript.Sliding());

                    break;
                case PlayerState.Dead:
                    playerMovementScript.handleDead();
                    footstepsAudioSource.Stop();

                    break;
                case PlayerState.Dancing:
                    playerMovementScript.handleDancing();
                    footstepsAudioSource.Stop();
                    break;
                default:

                    throw new ArgumentOutOfRangeException(nameof(newState), newState, null);
            }

            OnAfterStateChanged?.Invoke(newState);
        }
        else
        {
            Debug.Log("<color=red>Blocked PLAYERSTATE to change from [" + playerState + "] to [" + newState + "]</color>");
        }
    }





    public void TakeDamage(int amount)
    {
        anim.SetTrigger("IsHit");
        HP -= amount;
    }

     public void InstaDeath()
    {
        HP = 0;
    }

}

public enum PlayerState
{
    Idle = 0,
    Running = 1,
    Jumping = 2,
    Sliding = 3,
    Dead = 4,

    Dancing = 5,

}