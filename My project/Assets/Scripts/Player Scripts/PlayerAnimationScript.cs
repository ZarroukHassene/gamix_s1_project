using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAnimationScript :MonoBehaviour
{
    // Start is called before the first frame update

    Animator animator; // Animator of the player
    void Start()
    {
        animator = GetComponent<Animator>();
        //Init animator state machine
        animator.SetBool("IsDead", false);
        animator.SetBool("IsIdle", true);
        animator.SetBool("IsRunning", false);


        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void updateAnimation(string animationName)
    {
        if (animationName == "IsIdle")
        {
            animator.SetBool("IsDead", false);
            animator.SetBool("IsIdle", true);
            animator.SetBool("IsRunning", false);
        }
        if (animationName == "IsRunning")
        {
            animator.SetBool("IsDead", false);
            animator.SetBool("IsIdle", false);
            animator.SetBool("IsRunning", true);
        }
        if (animationName == "IsDead")
        {
            animator.SetBool("IsDead", true);
            animator.SetBool("IsIdle", false);
            animator.SetBool("IsRunning", false);
        }
    }
}
