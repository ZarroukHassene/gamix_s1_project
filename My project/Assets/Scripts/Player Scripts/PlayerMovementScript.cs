using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovementScript : MonoBehaviour
{
    private float horizontalInput;
    PlayerState playerState;
    public bool isGrounded;
    GameState State;
    Rigidbody rb; // Rigidbody of the player
    Animator anim; // Animator of the player

    [SerializeField] Vector3 StrafeForce = new Vector3(0.5f, 0, 0);
    [SerializeField] Vector3 AirStrafeForce = new Vector3(0.1f, 0, 0);
    [SerializeField] float JumpForce = 220;

    Vector3 originalColliderSize;
    Vector3 originalColliderCenter;
    public BoxCollider boxCollider;

    AudioSource footstepsAudioSource;
    AudioSource slidingAudioSource;
    AudioSource jumpingAudioSource;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        anim = GetComponent<Animator>();

        boxCollider = GetComponent<BoxCollider>();

        footstepsAudioSource = GameObject.Find("FootstepsSource").GetComponent<AudioSource>();
        slidingAudioSource = GameObject.Find("SlidingSource").GetComponent<AudioSource>();
        jumpingAudioSource = GameObject.Find("JumpingSource").GetComponent<AudioSource>();

        originalColliderSize = boxCollider.size;
        originalColliderCenter = boxCollider.center;
    }


    public void handleRunning()
    {
        anim.SetBool("IsRunning", true);

    }

    public void handleTurning()
    {
        horizontalInput = Input.GetAxis("Horizontal");
        if (horizontalInput != 0)
        {
            if (isGrounded)
            {
                rb.AddForce(horizontalInput * StrafeForce.x, 0, 0, ForceMode.Acceleration);
            }
            else //If player is in the air, apply less force
            {
                rb.AddForce(horizontalInput * AirStrafeForce.x, 0, 0, ForceMode.Force);
            }

            //Control "Run" animation blend tree
            anim.SetFloat("Turning", horizontalInput);
        }
        else
        {
            anim.SetFloat("Turning", 0);
        }
        //Handle sliding hitbox and rotation
        if (anim.GetCurrentAnimatorStateInfo(0).IsName("Slide") == true)
        {
            // Resize the collider to be half its current size
            boxCollider.size = originalColliderSize - new Vector3(0, 1, 0);
            // Move the center point of the collider down by half its current height
            boxCollider.center = originalColliderCenter - new Vector3(0, 0.5f, 0);

            //While sliding, rotate player according to horizontal input
            if (horizontalInput > 0)
            {   //DEbug log in red
                Debug.Log("<color=red>TURNING RIGHT > 0</color>");
                transform.rotation = Quaternion.Euler(0, 15, 0);
            }
            else if (horizontalInput < 0)
            {
                Debug.Log("<color=red>TURNING LEFT < 0</color>");
                transform.rotation = Quaternion.Euler(0, -15, 0);
            }
            else
            {
                transform.rotation = Quaternion.Euler(0, 0, 0);
            }

        }
        else
        {
            // Reset the collider size and center to their original values
            boxCollider.size = originalColliderSize;
            boxCollider.center = originalColliderCenter;
            transform.rotation = Quaternion.Euler(0, 0, 0);
        }
    }

    public void handleEnterJumping()
    {


        rb.AddForce(0, JumpForce, 0, ForceMode.Impulse);
        anim.SetTrigger("IsJumping");
        anim.SetBool("IsRunning", false);

    }

    public void handleExitJumping()
    {


        PlayerManager.Instance.ChangeState(PlayerState.Running);

    }

    public void handleEnterSliding()
    {
        anim.SetTrigger("IsSliding");
        anim.SetBool("IsRunning", false);
    }

    public void handleExitSliding()
    {
        PlayerManager.Instance.ChangeState(PlayerState.Running);

    }

    public void handleIdle()
    {
        anim.SetBool("IsIdle", true);
    }

    public void handleDead()
    {
        anim.SetBool("IsDead", true);
    }

    public void handleDancing()
    {
        anim.SetTrigger("Celebrate");
    }

    public void resetAnimatorBools()
    {
        // anim.SetBool("IsDead", false);
        // anim.SetBool("IsIdle", false);
        // anim.SetBool("IsRunning", false);
    }

    public bool CheckGrounded()
    {
        if (Physics.Raycast(transform.position, Vector3.down, 0.5f))
        {
            anim.SetBool("IsGrounded", true);
            return true;
        }
        else
        {
            anim.SetBool("IsGrounded", false);
            return false;

        }

    }

    //coroutine to handle entering and exiting sliding (depends on slide duration in seconds)
    public IEnumerator Sliding()
    {

        handleEnterSliding();

        //Wait for slide duration
        yield return new WaitForSeconds(1.3f);

        //Exit sliding
        handleExitSliding();
    }

    //Coroutine for jumping
    public IEnumerator Jumping()
    {
        handleEnterJumping();

        //Wait for jump duration
        yield return new WaitForSeconds(0.9f);

        handleExitJumping();
    }

}