using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainCameraScript : MonoBehaviour
{
    [SerializeField] GameObject playerObject;
    [SerializeField] Vector3 positionOffset = new Vector3(0.3f, 1.8f, -1f);
    [SerializeField] Vector3 rotationOffset = new Vector3(4.3f, 0, 0);

    [SerializeField] Vector3 slidingPositionOffset = new Vector3(0.3f, 0.18f, -0.8f);

    [SerializeField] Vector3 slidingRotationOffset = new Vector3(-35.5f, 0, 0);

    [SerializeField] float standingFOV = 110f;
    [SerializeField] float slidingFOV = 125f;

    [SerializeField] GameManager gameManager;
    [SerializeField] public Vector3 initialPosition = new Vector3(0.33f,12.449f, -101.66f);
    GameState gameState;

    //Animator
    Animator animator;

    //Vector3 cameraPositon;


    // Start is called before the first frame update
    void Start()
    {
        playerObject = GameObject.Find("Player");
        //Get animator
        animator = playerObject.GetComponent<Animator>();

        //Initialize camera position
        transform.position = initialPosition;

        transform.position = playerObject.transform.position + positionOffset;
        transform.rotation = playerObject.transform.rotation * Quaternion.Euler(rotationOffset);

        //Set fov
        GetComponent<Camera>().fieldOfView = standingFOV;

        //Set animator "Apply Root Motion" to true
        animator.applyRootMotion = false;

    }

    // Update is called once per frame
    void Update()
    {

        if (gameManager.State == GameState.InGame)
        { 
            //If animator current state is "IsSliding"
            if (playerObject.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("Slide"))
            {
                //Lerp
                transform.position = Vector3.Lerp(transform.position, playerObject.transform.position + slidingPositionOffset, 0.1f);
                transform.rotation = Quaternion.Lerp(transform.rotation, playerObject.transform.rotation * Quaternion.Euler(slidingRotationOffset), 0.1f);
                //FOV
                GetComponent<Camera>().fieldOfView = slidingFOV;
            }
            else
            { animator.applyRootMotion = false;

                transform.position = Vector3.Lerp(transform.position, playerObject.transform.position + positionOffset, 0.1f);
                transform.rotation = Quaternion.Lerp(transform.rotation, playerObject.transform.rotation * Quaternion.Euler(rotationOffset), 0.1f);
                //FOV
                GetComponent<Camera>().fieldOfView = standingFOV;
            }
        }
       
        





    }


}
