using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class LandmineScript : MonoBehaviour
{
    public GameEvent OnLandmineHit;

    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {

            PlayerManager.Instance.TakeDamage(25);
            transform.GetChild(0).GetComponent<Light>().intensity = 480;
            StartCoroutine(Explosion());
            //Play audiosource
            GetComponent<AudioSource>().Play();
            //Shake camera
            CameraShaker.Invoke();

        }
    }

    IEnumerator Explosion()
    {

        while (transform.GetChild(0).GetComponent<Light>().intensity > 1)
        {
            transform.GetChild(0).GetComponent<Light>().intensity -= 20;
            yield return new WaitForSeconds(0.1f);

        }
        Destroy(this.gameObject);

    }







}
