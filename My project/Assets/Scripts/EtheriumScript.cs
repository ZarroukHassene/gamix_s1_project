using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class EtheriumScript : MonoBehaviour
{
    public UnityEvent etheriumCollectEvent;

    [SerializeField] GameObject scoreNumber;


    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            etheriumCollectEvent.Invoke();
            //SetTrigger for EthCollected in "ScoreNumber" animator
            scoreNumber.GetComponent<Animator>().SetTrigger("EthCollected");
             GetComponent<AudioSource>().Play();
            //Set direct child to disabled
            transform.GetChild(0).gameObject.SetActive(false);
        }
    }

    private void Start()
    {
        //Get scoreNumber gameobject
        scoreNumber = GameObject.Find("ScoreNumber");
        etheriumCollectEvent.AddListener(GameObject.FindGameObjectWithTag("GameController").GetComponent<GameManager>().IncrementScore);
        //UpdateScore
        etheriumCollectEvent.AddListener(GameObject.Find("UIManager").GetComponent<UIManager>().UpdateScore);


    }

    //Event subscriber
    public void testMethod()
    {
        Debug.Log("Etherium collected");
    }
}
