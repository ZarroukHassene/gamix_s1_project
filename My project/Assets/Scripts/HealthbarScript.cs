using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthbarScript : MonoBehaviour
{
    [SerializeField] public Slider healthSlider;
    [SerializeField] public Slider easeHealthSlider;
    [SerializeField] public float maxHealth = 100f;
    [SerializeField] public float currentHealth = 100f;
    private float lerpSpeed = 0.1f;
    // Start is called before the first frame update
    void Start()
    {
        currentHealth = maxHealth;
    }

    // Update is called once per frame
    void Update()
    {
        //Get HP from player
        currentHealth = PlayerManager.Instance.HP;

        if(healthSlider.value != currentHealth)
        {
            healthSlider.value = currentHealth;
        }

        if(healthSlider.value != easeHealthSlider.value)
        {
            easeHealthSlider.value = Mathf.Lerp(easeHealthSlider.value, currentHealth, lerpSpeed);
        }

        
    }
}
