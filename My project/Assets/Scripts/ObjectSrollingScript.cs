using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Objects with this script will scroll on the Z axis with constant speed towards the fixed camera, 
//emulating movement on the highway level

public class ObjectSrollingScript : MonoBehaviour
{
    // Start is called before the first frame update
    public float ScrollingSpeed = 0.1f;
    GameManager gameManager;
    GameState State;

    private void Start()
    {
        gameManager = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameManager>();
            //Get gameState
            State = gameManager.State;
    }

    // Update is called once per frame
    void Update()
    {
        if( gameManager.State == GameState.InGame )
        {
            transform.Translate(0, 0, -ScrollingSpeed);
        }
        
        
        

    }
}
