using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;



public class FireColumnScript : MonoBehaviour
{

    GameManager gameManager;
    GameState State;

    Vector3 originalPosition;
    Vector3 targetPosition;

    [SerializeField] float speed = 16.0f;
    [SerializeField] float waitDuration = 1.0f;

    UnityEvent fireColumnCollideEvent;

    public GameEvent OnFireColumneHit;

    [SerializeField] bool inverted = false;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            PlayerManager.Instance.InstaDeath();
            //Get child and get its audio source and play t
            transform.GetChild(0).GetComponent<AudioSource>().Play();
            //Shake camera
            CameraShaker.Invoke();
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        gameManager = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameManager>();
        //Get gameState
        State = gameManager.State;
        //Get current position
        originalPosition = transform.position;
        //Set target position (which is the original position + 15 on x axis)
        if (!inverted)
        { targetPosition = new Vector3(originalPosition.x + 7.1f, originalPosition.y, originalPosition.z); }
        else
        { targetPosition = new Vector3(originalPosition.x - 7.1f, originalPosition.y, originalPosition.z); }

        StartCoroutine(TranslateObject());
    }

    //Coroutine to translate the object on the X axis until it reaches a specific position, holds 1 second then returns to its original position
    IEnumerator TranslateObject()
    {
        if (inverted)
        {
            while (true)
            {
                while (transform.position.x > targetPosition.x)
                {
                    transform.Translate(Vector3.left * Time.deltaTime * speed);
                    yield return null;

                }
                yield return new WaitForSeconds(1);
                while (transform.position.x < originalPosition.x)
                {
                    transform.Translate(Vector3.right * Time.deltaTime * speed);
                    yield return null;

                }
                yield return new WaitForSeconds(waitDuration);
            }
        }
        else
        {
            while (true)
        {
            while (transform.position.x < targetPosition.x)
            {
                transform.Translate(Vector3.right * Time.deltaTime * speed);
                yield return null;

            }
            yield return new WaitForSeconds(1);
            while (transform.position.x > originalPosition.x)
            {
                transform.Translate(Vector3.left * Time.deltaTime * speed);
                yield return null;

            }
            yield return new WaitForSeconds(waitDuration);
        }
        }
        
    }


}
