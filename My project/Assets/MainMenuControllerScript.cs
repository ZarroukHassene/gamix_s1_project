using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenuControllerScript : StaticInstance<MainMenuControllerScript>
{
    public void ButtonClicked_StartLevel1()
    {
        GameManager.Instance.ChangeState(GameState.StartLevel1);
    }
}
