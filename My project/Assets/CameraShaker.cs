using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using System;

public class CameraShaker : MonoBehaviour
{
    [SerializeField] private Vector3 _positionStrength;
    [SerializeField] private Vector3 _rotationStrength;

    private static event Action Shake;

    public static void Invoke(){
        Shake?.Invoke();
    }

    private void OnEnable() => Shake += CameraShake;
    private void OnDisable() => Shake -= CameraShake;

    // Start is called before the first frame update
    private void CameraShake(){
        transform.DOKill();
        transform.DOShakePosition(0.3f, _positionStrength);
        transform.DOShakeRotation(0.3f, _rotationStrength);
    }
}
