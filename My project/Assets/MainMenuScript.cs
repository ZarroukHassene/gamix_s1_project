using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenuScript : MonoBehaviour
{
    public void ButtonClicked_StartLevel1()
    {
        GameManager.Instance.ChangeState(GameState.StartLevel1);
    }
}
