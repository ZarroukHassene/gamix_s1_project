using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorLerp : MonoBehaviour
{
    public Material material;
    public Color[] colors;
    private int currentColorIndex = 0;
    private int targetColorIndex = 1;
    private float targetPoint;
    public float time;


    // Update is called once per frame
    void Update()
    {
        Transition();
    }

    void Transition()
    {
        targetPoint += Time.deltaTime / time;
        //Change the emission color
        material.SetColor("_EmissionColor", Color.Lerp(colors[currentColorIndex], colors[targetColorIndex], targetPoint));
        
        if (targetPoint >= 1f)
        {
            targetPoint = 0f;
            currentColorIndex = targetColorIndex;
            targetColorIndex++;
            if (targetColorIndex >= colors.Length)
            {
                targetColorIndex = 0;
            }
        }
    }
}
